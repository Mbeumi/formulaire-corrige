-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  sam. 19 juin 2021 à 13:39
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `formulaire_inch_class`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `date_naissance` datetime NOT NULL,
  `pays` varchar(255) NOT NULL,
  `telephone` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `sexe`, `date_naissance`, `pays`, `telephone`, `email`, `pass`, `photo`) VALUES
(4, 'dgrshg', 'pommpl', 'Femme', '2021-06-28 00:00:00', 'Tchad', 2147483647, 'franckymbeums@yahoo.com', 'g,nh, h,', '20210618_055247.png'),
(5, 'glkkmÃ¹ll', 'pommpl', 'Homme', '2021-06-22 00:00:00', 'Algerie', 2147483647, 'franckymbeums@gmail.com', 'jfrkguhjol4254', 'SL_060521_43530_02.jpg'),
(8, 'hnhgj', 'jtgtjkyfh', 'Femme', '2021-06-30 00:00:00', 'Canada', 2147483647, 'jdjnfjfjf@yahoo.com', 'dasfq123', '20210618_055247.png'),
(9, 'hg', 'qq', 'Homme', '2021-07-02 00:00:00', 'Congo', 2632, 'jdjnjf@yahoo.com', 'uyil;74825', 'img_franck.jpeg'),
(11, 'hg', 'qq', 'Homme', '2021-07-02 00:00:00', 'Congo', 2632, 'jdjf@yahoo.com', 'gjhk,jh!5241', 'img_franck.jpeg'),
(13, 'hgjkh;h42', 'qq523', 'Homme', '2021-07-02 00:00:00', 'Congo', 2632, 'jdjlmk455f@yahoo.com', 'kj;ikj542354', 'img_franck.jpeg'),
(14, 'hgjkh;h42', 'qq523', 'Homme', '2021-07-02 00:00:00', 'Congo', 263276753, 'jdjlmk4fh55f@yahoo.com', '787542874imhk', '20210618_052818.jpg'),
(15, 'hgjkh;h4252b', 'qq523hgn', 'Femme', '2021-07-12 00:00:00', 'Congo', 2147483647, 'jdjlmjhj541k4fh55f@yahoo.com', 'hg,jh52447', '20210618_052818.jpg'),
(16, 'hgjkh;h4', 'qq523hg', 'Femme', '2021-08-06 00:00:00', 'Congo', 2147483647, 'jdjlmjhj5k4fh55f@yahoo.com', 'gcchftdrsetze1324231', '20210618_052818.jpg'),
(17, 'hgjkhisj', 'q23hg', 'Femme', '2021-08-06 00:00:00', 'Cameroun', 2147483647, 'jdjlmjhfh55f@yahoo.com', 'sWCqdv3244', 'IMG-20210522-WA0008.jpg'),
(19, 'hgjkhisj', 'q23hg', 'Femme', '2021-08-06 00:00:00', 'Cameroun', 2147483647, 'jdjlmjh55f@yahoo.com', 'vfrfr', 'IMG-20210522-WA0008.jpg'),
(21, 'hgjkhisj', 'q23hg', 'Femme', '2021-08-06 00:00:00', 'Cameroun', 2147483647, 'jdjlmjh5@yahoo.com', 'dfgvsf', 'IMG-20210522-WA0008.jpg'),
(23, 'hgjkhisj', 'q23hg', 'Femme', '2021-08-06 00:00:00', 'Cameroun', 2147483647, 'jdjlmjh5544@yahoo.com', 'hgfhfhh', '688.jpg'),
(24, 'hgjkhisj4546612', 'q23hg44', 'Homme', '2021-08-10 00:00:00', 'Cote Ivoire', 2147483647, 'jd5544@yahoo.com', 'aee\"zÃ©5845454', '688.jpg'),
(26, 'MBEUMI', 'FRANCK', 'Homme', '2021-06-06 00:00:00', 'Cameroun', 695, 'franckymbeums12@gmail.com', 'pmoner14', 'img_franck.png'),
(28, 'hgjkh;h42', 'qqqqqq', 'Homme', '2021-06-21 00:00:00', 'Cameroun', 2147483647, 'vcjgcfh@yahoo.fr', 'tdhfgjhfghgfg', '20210618_055617.png'),
(29, 'hg', 'qq', 'Homme', '2021-06-23 00:00:00', 'Inde', 2147483647, 'qqqq@yahoo.fr', 'djhfhef3323445', 'images (5).jpeg'),
(30, 'hg dd', 'qq', 'Homme', '2021-06-23 00:00:00', 'Inde', 2147483647, 'qqqaaq@yahoo.fr', 'erhdtjhy567', 'images (5).jpeg'),
(31, 'hhgdfeghg jhdehkjh hheh', 'qq', 'Homme', '2021-06-23 00:00:00', 'Inde', 2147483647, 'qqqgdfhaaq@yahoo.fr', 'zgetryhr-t-', 'images (5).jpeg'),
(33, 'hg', 'qqqqqq', 'Homme', '2021-06-16 00:00:00', 'Algerie', 2147483647, 'qqq574545q@yahoo.fr', 'tkyhouy7678', 'â€”Pngtreeâ€”king crown for symbol and_3869393.png'),
(35, 'hg', 'qqqqqq', 'Homme', '2021-06-16 00:00:00', 'Algerie', 2147483647, 'qqq574hgfg545q@yahoo.fr', 'jhgf', 'â€”Pngtreeâ€”king crown for symbol and_3869393.png'),
(37, 'hg', 'qqqqqq', 'Homme', '2021-06-16 00:00:00', 'Algerie', 2147483647, 'q74hgfg545q@yahoo.fr', 'hgfjt55274', 'â€”Pngtreeâ€”king crown for symbol and_3869393.png'),
(39, 'hg', 'qqqqqq', 'Homme', '2021-06-16 00:00:00', 'Algerie', 2147483647, 'q74hgfhgjgkfg545q@yahoo.fr', 'gf,xf4', 'â€”Pngtreeâ€”king crown for symbol and_3869393.png');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
